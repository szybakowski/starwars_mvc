package com.Szyba.SpringTraining.Domain;

import org.junit.Assert;
import org.junit.Test;

public class ClonesTests {
    @Test
    public void SetGetSectionNameTests() {
        Clones clones = getClones();

        clones.setSectionName("Stormtrooper");

        Assert.assertEquals("Stormtrooper", clones.getSectionName());
    }

    private Clones getClones() {
        Clones clones = new Clones();
        clones.setSectionType("Trooper");
        clones.setSectionQuentity(300);
        return clones;
    }
}
