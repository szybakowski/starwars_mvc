package com.Szyba.SpringTraining.Domain.Repository;

import com.Szyba.SpringTraining.Domain.Clones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Jawa on 19.03.2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ClonesRepositoryTests {

    @Autowired
    private  ClonesRepository clonesRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void saveClonesTest(){
        Clones clones = getClones();
        entityManager.persist(clones);
        Clones getFromDB = clonesRepository.findOne(clones.getId());

        assertThat(clones).isEqualTo(getFromDB);
    }
    private Clones getClones() {
        Clones clones = new Clones();
        clones.setSectionName("Stormtrooper");
        clones.setSectionType("Trooper");
        clones.setSectionQuentity(300);
        return clones;
    }

}
