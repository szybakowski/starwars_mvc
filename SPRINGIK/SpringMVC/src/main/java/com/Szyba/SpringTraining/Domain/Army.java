package com.Szyba.SpringTraining.Domain;

import javax.persistence.*;

@Entity
public class Army {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @OneToOne
    private Jedi jedi;

    @OneToOne
    private  Droids droids;

    @OneToOne
    private Clones clones;

    public Army() {
    }

    public Jedi getJedi() {
        return jedi;
    }

    public void setJedi(Jedi jedi) {
        this.jedi = jedi;
    }

    public Droids getDroids() {
        return droids;
    }

    public void setDroids(Droids droids) {
        this.droids = droids;
    }

    public Clones getClones() {
        return clones;
    }

    public void setClones(Clones clones) {
        this.clones = clones;
    }
}
