package com.Szyba.SpringTraining.Services;

import com.Szyba.SpringTraining.Domain.Droids;
import com.Szyba.SpringTraining.Domain.Jedi;
import com.Szyba.SpringTraining.Domain.Repository.DroidsRepository;
import com.Szyba.SpringTraining.Domain.Repository.JediRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DroidsService {

    @Autowired
    DroidsRepository droidsRepository;

    @Autowired
    JediRepository jediRepository;

    public void createDroids(Droids droids) {
        droidsRepository.save(droids);
    }

    public List<Droids> getAllDroids() {
        List<Droids> alldroids = new ArrayList<Droids>();
        Iterable<Droids> droidsIterable = droidsRepository.findAll();

        for (Droids droids : droidsIterable) {
            alldroids.add(droids);
        }

        return alldroids;
    }

    public void deleteDroids(Long id) {
        droidsRepository.delete(id);
    }

    public void assignCommander(Droids droids) {
        droidsRepository.save(droids);
    }

    public Droids getOneDroids(Long id) {
        return droidsRepository.findOne(id);
    }
}
