package com.Szyba.SpringTraining.Domain.Repository;

import com.Szyba.SpringTraining.Domain.Army;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArmyRepository extends CrudRepository <Army, Long> {
}
