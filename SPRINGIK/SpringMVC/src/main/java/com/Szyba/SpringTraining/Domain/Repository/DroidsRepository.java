package com.Szyba.SpringTraining.Domain.Repository;

import com.Szyba.SpringTraining.Domain.Droids;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroidsRepository extends CrudRepository<Droids, Long>{
}
