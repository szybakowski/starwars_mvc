package com.Szyba.SpringTraining.Services;

import com.Szyba.SpringTraining.Domain.Clones;
import com.Szyba.SpringTraining.Domain.Repository.ClonesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClonesService {

    @Autowired
    ClonesRepository clonesRepository;

    public void createNewClonesSection(Clones clones) {
        Clones newClones = new Clones();
        newClones.setSectionName(clones.getSectionName());
        newClones.setSectionType(clones.getSectionType());
        newClones.setSectionQuentity(clones.getSectionQuentity());

        clonesRepository.save(newClones);
    }

    public List<Clones> allClones() {
        List<Clones> allClones = new ArrayList<Clones>();
        Iterable<Clones> clonesIterable = clonesRepository.findAll();

        for (Clones clones : clonesIterable) {
            allClones.add(clones);
        }

        return allClones;
    }

    public void deleteClones(Long id) {
        clonesRepository.delete(id);
    }

    public Clones getOneClones(Long id) {
        return clonesRepository.findById(id);
    }

    public void assignCommander(Clones clones) {
        clonesRepository.save(clones);
    }
}
