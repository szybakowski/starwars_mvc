package com.Szyba.SpringTraining.Domain.Repository;

import com.Szyba.SpringTraining.Domain.Jedi;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

@Repository
public class JediRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void createJedi(String name, String side, int age, String speeder) {
        Jedi newJedi = new Jedi(name,side,age,speeder);
        entityManager.persist(newJedi);
    }

    @Transactional
    public Collection<Jedi> getAllJedi() {
        return entityManager.createQuery("from Jedi", Jedi.class).getResultList();
    }

    @Transactional
    public void updateJedi(Jedi jedi) {
        entityManager.merge(jedi);
    }

    @Transactional
    public void deleteJedi(Jedi jedi) {
        entityManager.remove(jedi);
    }

    public Jedi getOneJedi(Long id) {
        Jedi jedi = entityManager.createQuery("from Jedi j where j.id=:id", Jedi.class)
                .setParameter("id", id).getSingleResult();

        return jedi;
    }
}
