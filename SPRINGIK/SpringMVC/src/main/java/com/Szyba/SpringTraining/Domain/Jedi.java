package com.Szyba.SpringTraining.Domain;

import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Jedi {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min=4, max=30)
    private String name;

    @NotNull
    @Size(min=4, max=30)
    private String side;

    @NotNull
    @Range(min=16, max=100)
    private int age;

    @Size(min=4, max=30)
    private String speeder;

    public Jedi() {
    }

    public Jedi(String name, String side, int age, String speeder) {
        this.name = name;
        this.side = side;
        this.age = age;
        this.speeder = speeder;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSpeeder() {
        return speeder;
    }

    public void setSpeeder(String speeder) {
        this.speeder = speeder;
    }

    public String toString(){
        return  name;
    }
}
