package com.Szyba.SpringTraining.Controller;

import com.Szyba.SpringTraining.Domain.Clones;
import com.Szyba.SpringTraining.Services.ClonesService;
import com.Szyba.SpringTraining.Services.JediService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class ClonesController {

    @Autowired
    ClonesService clonesService;

    @Autowired
    JediService jediService;

    @RequestMapping("/all_clones")
    public String getAllClones(Model model) {
        model.addAttribute("allClones" , clonesService.allClones() );

        return "all_clones";
    }

    @RequestMapping("/new_clones")
    public String goToFormNewClones(Model model) {
        model.addAttribute("clones", new Clones());

        return "new_clones";
    }

    @RequestMapping(value = "/saveClones", method = RequestMethod.POST)
    public String saveClones(@Valid Clones clones, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "new_clones";
        }

        clonesService.createNewClonesSection(clones);

        return "redirect:/all_clones";
    }

    @RequestMapping(value = "/delete_clones/{id}")
    public String deleteClones(@PathVariable("id") Long id) {
        clonesService.deleteClones(id);

        return "redirect:/all_clones";
    }

    @RequestMapping(value = "/assign_commander_clones/{idClones}")
    public String assignCommander(@PathVariable("idClones") Long idClones, Model model) {
        Clones clones = clonesService.getOneClones(idClones);
        model.addAttribute("clones",clones );
        model.addAttribute("allJedi", jediService.getAllJedi());

        return "assign_commander_clones";
    }

    @RequestMapping(value = "/assign_commander_clones", method = RequestMethod.POST)
    public String assignCommander(Clones clones) {
        clonesService.assignCommander(clones);

        return "redirect:/all_clones";
    }

}
