package com.Szyba.SpringTraining;

import com.Szyba.SpringTraining.Domain.Clones;
import com.Szyba.SpringTraining.Domain.Repository.ClonesRepository;
import com.Szyba.SpringTraining.Domain.Repository.JediRepository;
import com.Szyba.SpringTraining.Domain.Repository.RoleRepository;
import com.Szyba.SpringTraining.Domain.Repository.UserRepository;
import com.Szyba.SpringTraining.Domain.Role;
import com.Szyba.SpringTraining.Domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Hello implements CommandLineRunner {

    @Override
    public void run(String... strings) throws Exception {
    }
}
