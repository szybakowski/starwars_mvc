package com.Szyba.SpringTraining.Services;

import com.Szyba.SpringTraining.Domain.Jedi;
import com.Szyba.SpringTraining.Domain.Repository.JediRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Component
public class JediService {

    @Autowired
    JediRepository jediRepository;

    public void createJedi(String name, String side, int age, String speeder) {
        jediRepository.createJedi(name, side, age, speeder);
    }

    public List<Jedi> getAllJedi() {
        return new ArrayList<>(jediRepository.getAllJedi());
    }

    public void updateJedi(Jedi updatedJedi) {
        jediRepository.updateJedi(updatedJedi);
    }

    public void deleteJedi(Jedi jedi) {
        jediRepository.deleteJedi(jedi);
    }

    public Jedi getOneJedi(Long id) {
       return jediRepository.getOneJedi(id);
    }
}
