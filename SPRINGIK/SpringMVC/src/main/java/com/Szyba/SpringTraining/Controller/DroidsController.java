package com.Szyba.SpringTraining.Controller;

import com.Szyba.SpringTraining.Domain.Droids;
import com.Szyba.SpringTraining.Services.DroidsService;
import com.Szyba.SpringTraining.Services.JediService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class DroidsController {

    @Autowired
    DroidsService droidsService;

    @Autowired
    JediService jediService;

    @RequestMapping(value = "/all_droids", method = RequestMethod.GET)
    public String AllDroids(Model model) {
        model.addAttribute("allDroids", droidsService.getAllDroids());

        return "/all_droids";
    }

    @RequestMapping(value = "/new_droids", method = RequestMethod.GET)
    public String goToFormNewDroids(Model model) {
        model.addAttribute("droids", new Droids());

        return "new_droids";
    }

    @RequestMapping(value = "/save_droids", method = RequestMethod.POST)
    public String saveDroids(@Valid Droids droids, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "new_droids";
        }

        droidsService.createDroids(droids);

        return "redirect:/all_droids";
    }

    @RequestMapping(value = "/delete_droids/{id}")
    public String deleteDroids(@PathVariable("id") Long id) {
        droidsService.deleteDroids(id);

        return "redirect:/all_droids";
    }

    @RequestMapping(value = "/assign_commander_droids", method = RequestMethod.POST)
    public String assignCommander(Droids droids) {
        droidsService.assignCommander(droids);

        return "redirect:/all_droids";
    }

    @RequestMapping(value = "/assign_commander_droids/{idDroids}")
    public String goToForm(@PathVariable("idDroids") Long idDroids, Model model) {
        Droids droid = droidsService.getOneDroids(idDroids);
        model.addAttribute("allJedi", jediService.getAllJedi());
        model.addAttribute("droids", droid);

        return "assign_commander_droids";
    }
}
