package com.Szyba.SpringTraining.Domain.Repository;

import com.Szyba.SpringTraining.Domain.Clones;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClonesRepository extends CrudRepository<Clones, Long> {
    Clones findById(Long id);
    Clones findBySectionName(String sectionName);
}
