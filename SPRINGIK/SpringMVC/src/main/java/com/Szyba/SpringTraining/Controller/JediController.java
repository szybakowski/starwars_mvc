package com.Szyba.SpringTraining.Controller;

import com.Szyba.SpringTraining.Domain.Jedi;
import com.Szyba.SpringTraining.Services.JediService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Controller
public class JediController {

    @Autowired
    JediService jediService;

    @RequestMapping("/all_jedi")
    public String getAllJedi(Model model) {
        List<Jedi> listOfAllJedi = jediService.getAllJedi();
        model.addAttribute("allJedi", listOfAllJedi  );

        return "all_jedi";
    }

    @RequestMapping("/new_jedi")
    public String goToFormNewJedi(Model model) {
        model.addAttribute("jedi", new Jedi());

        return "new_jedi";
    }

    @RequestMapping(value = "/saveJedi", method = RequestMethod.POST)
    public String saveJedi(@Valid Jedi jedi, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "new_jedi";
        }

        jediService.createJedi(jedi.getName(), jedi.getSide(), jedi.getAge(), "none");

        return "redirect:/all_jedi";
    }
    @RequestMapping(value = "/delete_jedi/{idJedi}")
    public String deleteJedi(@PathVariable("idJedi") Long idJedi) {
        try {
            Jedi jedi = jediService.getOneJedi(idJedi);
            jediService.deleteJedi(jedi);
        } catch(Exception e) {
            System.out.println(e.toString());
        }

        return "redirect:/all_jedi";
    }
}
