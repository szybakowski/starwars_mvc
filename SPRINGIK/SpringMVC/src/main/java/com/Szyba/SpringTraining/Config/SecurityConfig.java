package com.Szyba.SpringTraining.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    DataSource dataSource;

    @Override
    public void configure(HttpSecurity security) throws Exception {

        security.authorizeRequests()
                .antMatchers("/all_jedi").hasAnyAuthority("USER", "ADMIN")
                .antMatchers("/assign_commander_clones/**").hasAnyAuthority("ADMIN")
                .antMatchers("/assign_commander_droids/**").hasAnyAuthority("ADMIN")
                .antMatchers("/delete_clones/**").hasAnyAuthority("ADMIN")
                .antMatchers("/delete_droids/**").hasAnyAuthority("ADMIN")
                .antMatchers("/delete_jedi/**").hasAnyAuthority("ADMIN")
                .antMatchers("/new_clones").hasAnyAuthority("ADMIN")
                .antMatchers("/new_droids").hasAnyAuthority("ADMIN")
                .antMatchers("/new_jedi").hasAnyAuthority("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin().defaultSuccessUrl("/all_jedi");

    }

    @Autowired
    public void securityUsers(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("Bartucha").password("bartucha").roles("ADMIN")
//                .and()
//                .withUser("Guest").password("guest").roles("USER");
//    }
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("SELECT username,password,enabled FROM USER WHERE username=?")
                .authoritiesByUsernameQuery("SELECT username,role FROM ROLE WHERE username= ?");
    }
}
