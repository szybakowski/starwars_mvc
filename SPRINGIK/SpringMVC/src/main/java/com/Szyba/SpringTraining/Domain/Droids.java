package com.Szyba.SpringTraining.Domain;

import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Droids {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min=4, max=30)
    private String sectionName;

    @NotNull
    @Size(min=4, max=30)
    private String sectionType;

    @NotNull
    @Range(min=50, max=100000)
    private int sectionQuentity;

    @OneToOne
    private Jedi sectionCommander;

    public Droids() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionType() {
        return sectionType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public int getSectionQuentity() {
        return sectionQuentity;
    }

    public void setSectionQuentity(int sectionQuentity) {
        this.sectionQuentity = sectionQuentity;
    }

    public Jedi getSectionCommander() {
        return sectionCommander;
    }

    public void setSectionCommander(Jedi sectionCommander) {
        this.sectionCommander = sectionCommander;
    }
}
